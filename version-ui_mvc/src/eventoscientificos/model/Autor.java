/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Autor implements Serializable {
    
    private Utilizador utilizador;
    private String strNome;
    private String strAfiliacao;
    private String strEmail;
    
    public Autor()
    {
        this.utilizador = null;
    }
    
    public void setNome(String strNome)
    {
        this.strNome = strNome;
    }
    
    public void setAfiliacao(String strAfiliacao)
    {
        this.strAfiliacao = strAfiliacao;
    }
    
    public void setEMail(String strEMail)
    {
        this.strEmail = strEMail;
    }
    
    public void setUtilizador(Utilizador utilizador)
    {
        this.utilizador = utilizador;
    }
    
    public boolean valida()
    {
        return true;
    }

    boolean podeSerCorrespondente() 
    {
        return (utilizador != null);
    }
    
    @Override
    public String toString()
    {
        return this.strNome + " - " + this.strAfiliacao +  " - " + this.strEmail; 
    }
}
