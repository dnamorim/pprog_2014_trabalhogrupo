/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Mecanismo1 implements MecanismoDistribuicao {

    /**
     *
     */
    public Mecanismo1() {

    }

    @Override
    public void distribuir(ProcessoDistribuicao pd) {
        List<Submissao> listSub = pd.getEvento().getListaSubmissoes();
        
        List<Revisor> listRev = pd.getEvento().getCP().getListaRevisores();
        
        boolean adicionarRev=false;
        
        for (Submissao s : listSub) {
            adicionarRev=false;
            Distribuicao d = new Distribuicao();
            d.setArtigo(s.getArtigo());
           for (Revisor r : listRev) {
                for (Topico t : r.getListaTopicos()) {
                    if(s.getArtigo().getListTopicosArtigo().contains(t)) {
                        adicionarRev=true;
                    }
                }
                
                if (adicionarRev == true && d.getListRevisor().size() < 3) {
                    d.addRevisor(r);
                }
            }
           pd.novaDistribuicao(d);
        }
        
    }

    @Override
    public String descricaoMecanismo() {
        return "Mecanismo 1: Todos os topicos tem preferencialmente 3 revisores";
    }

}
