package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */
public final class Utilizador implements Serializable {

    private String strNome;
    private String strUsername;
    private String strPassword;
    private String strEmail;

    public Utilizador() {
    }

    public Utilizador(String username, String pwd, String nome, String email) {
        this.setUsername(username);
        this.setPassword(pwd);
        this.setNome(nome);
        this.setEmail(email);
    }

    public final void setNome(String strNome) {
        this.strNome = strNome;
    }

    public final void setUsername(String strUsername) {
        this.strUsername = strUsername;
    }

    public final void setPassword(String strPassword) {
        this.strPassword = strPassword;
    }

    public void setEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public boolean valida() {
        return true;
    }

    // método alterado na iteração 2
    public boolean mesmoQueUtilizador(Utilizador u) {
        if (getUsername().equals(u.getUsername())
                || getEmail().equals(u.getEmail())) {
            return true;
        } else {
            return false;
        }
    }

    // método adicionado na iteração 2
    public String getNome() {
        return strNome;
    }

    public String getUsername() {
        return strUsername;
    }

    public String getEmail() {
        return strEmail;
    }

    public String getPassword() {
        return this.strPassword;
    }

    @Override
    public String toString() {
        String str = this.strNome + " (" + this.strUsername.trim() + ")";
        return str;
    }

}
