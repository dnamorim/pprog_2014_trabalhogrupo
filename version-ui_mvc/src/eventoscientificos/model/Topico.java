/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author iazevedo
 */
public class Topico implements Serializable {
    
    /**
     * Descrição Tópico
     */
    private String m_strDescricao;
    
    /**
     * CódigoACM Tópico
     */
    private String m_strCodigoACM;

   
    /**
     * Define a descrição do tópico
     * @param strDescricao descricao tópico
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }


    /**
     * Define o código ACM do tópico
     * @param codigoACM codigoACM a definir
     */
    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }
    
    public String getCodigoACM() {
        return this.m_strCodigoACM;
    }
    
    /**
     * Valida Tópico inserido
     * @return true=válido/false=inválido
     */
    public boolean valida()
    {
        return true;
    }
    
    /**
     * Devolve descrição de tópico
     * @return descricao topico
     */
    @Override
    public String toString()
    {
        return this.m_strDescricao+" ("+this.m_strCodigoACM+")";
    }
    
    
}
