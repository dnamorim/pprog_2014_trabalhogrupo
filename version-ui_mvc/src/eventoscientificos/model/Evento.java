/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Nuno Silva
 */
public class Evento implements Serializable {

    private String strTitulo;
    private String strDescricao;
    private Local local;
    private String strDataInicio;
    private String strDataFim;
    private String strDataLimiteSubmissão;
    private List<Organizador> listOrganizadores;
    private List<Submissao> listSubmissoes;
    private CP cp;
    private List<Topico> listTopicos;
    private ProcessoDistribuicao processoDistribuicao;
    private boolean distAplicada;

    public Evento()
    {
        local = new Local();
        listOrganizadores = new ArrayList<Organizador>();
        listSubmissoes = new ArrayList<Submissao>();
        listTopicos = new ArrayList<Topico>();
        distAplicada = false;
    }

    public Evento(String titulo, String descricao)
    {
        local = new Local();
        listOrganizadores = new ArrayList<Organizador>();
        listSubmissoes = new ArrayList<Submissao>();
        listTopicos = new ArrayList<Topico>();
        this.setTitulo(titulo);
        this.setDescricao(descricao);
        distAplicada = false;
    }

    public CP novaCP()
    {
        cp = new CP();

        return getCp();
    }

    public final void setTitulo(String strTitulo)
    {
        this.strTitulo = strTitulo;
    }

    public final void setDescricao(String strDescricao)
    {
        this.strDescricao = strDescricao;
    }

    public void setDataInicio(String strDataInicio)
    {
        this.strDataInicio = strDataInicio;
    }

    public void setDataFim(String strDataFim)
    {
        this.strDataFim = strDataFim;
    }

    // adicionada na iteração 2
    public void setDataLimiteSubmissão(String strDataLimiteSubmissão)
    {
        this.strDataLimiteSubmissão = strDataLimiteSubmissão;
    }

    public void setLocal(String strLocal)
    {
        this.getLocal().setLocal(strLocal);
    }

    public List<Organizador> getListaOrganizadores()
    {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = getListOrganizadores().listIterator(); it.hasNext();)
        {
            lOrg.add(it.next());
        }

        return lOrg;
    }

    public boolean addOrganizador(String strId, Utilizador u)
    {
        Organizador o = new Organizador(strId, u);

        o.valida();

        return addOrganizador(o);
    }

    private boolean addOrganizador(Organizador o)
    {
        return getListOrganizadores().add(o);
    }

    public boolean valida()
    {
        return true;
    }

    public void setCP(CP cp)
    {
        cp = cp;
    }

    @Override
    public String toString()
    {
        return this.getStrTitulo() + "+ ...";
    }

    public boolean aceitaSubmissoes()
    {
        return true;
    }

    public Submissao novaSubmissao()
    {
        return new Submissao();
    }

    public boolean addSubmissao(Submissao submissao)
    {
        if (validaSubmissao(submissao))
        {
            return this.getListSubmissoes().add(submissao);
        } else
        {
            return false;
        }
    }

    private boolean validaSubmissao(Submissao submissao)
    {
        return submissao.valida();
    }

    public List<Submissao> getListaSubmissoes()
    {
        return this.getListSubmissoes();
    }

    public CP getCP()
    {
        return this.getCp();
    }
    
    public List<Topico> getTopicos()
    {
        return getListTopicos();
    }

    // adicionada na iteração 2
    public Topico novoTopico()
    {
        return new Topico();
    }

    // adicionada na iteração 2
    public boolean addTopico(Topico t)
    {
        return this.getListTopicos().add(t);
    }

    // adicionada na iteração 2
    public boolean validaTopico(Topico t)
    {
        if (t.valida() && validaGlobalTopico(t))
        {
            return true;
        } else
        {
            return false;
        }
    }

    // adicionada na iteração 2
    private boolean validaGlobalTopico(Topico t)
    {
        return true;
    }

    /**
     * @return the strTitulo
     */
    public String getStrTitulo() {
        return strTitulo;
    }

    /**
     * @return the strDescricao
     */
    public String getStrDescricao() {
        return strDescricao;
    }

    /**
     * @return the local
     */
    public Local getLocal() {
        return local;
    }

    /**
     * @return the strDataInicio
     */
    public String getStrDataInicio() {
        return strDataInicio;
    }

    /**
     * @return the strDataFim
     */
    public String getStrDataFim() {
        return strDataFim;
    }

    /**
     * @return the strDataLimiteSubmissão
     */
    public String getStrDataLimiteSubmissão() {
        return strDataLimiteSubmissão;
    }

    /**
     * @return the listOrganizadores
     */
    public List<Organizador> getListOrganizadores() {
        return listOrganizadores;
    }

    /**
     * @return the listSubmissoes
     */
    public List<Submissao> getListSubmissoes() {
        return listSubmissoes;
    }

    /**
     * @return the cp
     */
    public CP getCp() {
        return cp;
    }

    /**
     * @return the listTopicos
     */
    public List<Topico> getListTopicos() {
        return listTopicos;
    }

    /**
     * @return the processoDistribuicao
     */
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return processoDistribuicao;
    }

    /**
     * @return the distAplicada
     */
    public boolean isDistAplicada() {
        return distAplicada;
    }
}
