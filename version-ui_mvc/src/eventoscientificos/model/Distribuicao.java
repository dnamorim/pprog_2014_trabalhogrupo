/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Distribuicao implements Serializable {
    
    private Artigo artigo;
    private List<Revisor> listRevisoresArtigo;
    
    public Distribuicao() {
        this.listRevisoresArtigo = new ArrayList<Revisor>();
    }
    
    public Distribuicao(Artigo artigo, List<Revisor> listRevisores) {
        this.artigo = artigo;
        this.listRevisoresArtigo = listRevisores;
    }
    
    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }
    
    public boolean addRevisor(Revisor revisor) {
        return this.listRevisoresArtigo.add(revisor);
    }
    
    public List<Revisor> getListRevisor() {
        return this.listRevisoresArtigo;
    }
    

}
