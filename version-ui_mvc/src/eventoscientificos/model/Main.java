/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.ui.*;



/**
 *
 * @author Nuno Silva
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        try
        {
            Empresa empresa = new Empresa();

            EventosCientificosUI uiMenu = new EventosCientificosUI(empresa);
            uiMenu.setVisible(true);

        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }

}
