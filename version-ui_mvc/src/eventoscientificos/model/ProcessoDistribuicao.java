/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class ProcessoDistribuicao implements Serializable {

    private MecanismoDistribuicao mecanismo;
    private List<Distribuicao> listDistribuicao;
    private Evento evento;
    
    public ProcessoDistribuicao() {
        listDistribuicao = new ArrayList<Distribuicao>();
    }

    public void setListDistribuicao(List<Distribuicao> listDistribuicao) {
        this.listDistribuicao = listDistribuicao;
    }
    
    public void novaDistribuicao(Distribuicao d) {
        this.listDistribuicao.add(d);
    }
    
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    public List<Distribuicao> getProcessoDistribuicao() {
        return this.listDistribuicao;
    }
    
    public void setMecanismoDistribuicao(MecanismoDistribuicao mecanismo) {
        this.mecanismo = mecanismo;
    }
    
    public Evento getEvento() {
        return evento;
    }
    
    public void distribuir() {
        this.mecanismo.distribuir(this);
    }
    
}

