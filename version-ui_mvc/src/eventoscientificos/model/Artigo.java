/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo implements Serializable {
    private String strTitulo;
    private String strResumo;
    private List<Autor> listaAutores;
    private Autor autorCorrespondente;
    private String strFicheiro;
    private List<Topico> listaTopicos;
    
    public Artigo()
    {
        listaAutores = new ArrayList<Autor>();
        listaTopicos = new ArrayList<Topico>();
    }
    
    public void setTitulo(String strTitulo)
    {
        this.strTitulo = strTitulo;
    }
    
    public void setResumo(String strResumo)
    {
        this.strResumo = strResumo;
    }
    
    public Autor novoAutor(String strNome, String strAfiliacao)
    {
        Autor autor = new Autor();
        
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        
        return autor;
    }
    
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail, Utilizador utilizador)
    {
        Autor autor = new Autor();
        
        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);
        autor.setUtilizador(utilizador);
        
        return autor;
    }
    
    public boolean addAutor(Autor autor)
    {
        if (validaAutor(autor))
            return listaAutores.add(autor);
        else
            return false;
    
    }

    private boolean validaAutor(Autor autor) 
    {
        return autor.valida();
    }
    
    public List<Autor> getPossiveisAutoresCorrespondentes()
    {
        List<Autor> la = new ArrayList<Autor>();
        
        for(Autor autor:this.listaAutores)
        {
           if (autor.podeSerCorrespondente())
           {
               la.add(autor);
           }    
        }
        return la;
    }
    
    public void setAutorCorrespondente(Autor autor)
    {
        this.autorCorrespondente=autor;
    }
    
    public void setFicheiro(String strFicheiro)
    {
        this.strFicheiro = strFicheiro;
    }
    
    // adicionado na iteração 2
    public void setListaTopicos(List<Topico> listaTopicos)
    {
        this.listaTopicos.addAll( listaTopicos );
    }
    
    public String getInfo()
    {
        return this.toString();
    }
    
    public boolean valida()
    {
        return true;
    }
    
    public List<Topico> getListTopicosArtigo() {
        return this.listaTopicos;
    }
     
 
    @Override
    public String toString()
    {
        return this.strTitulo + "+ ...";
    }
}
