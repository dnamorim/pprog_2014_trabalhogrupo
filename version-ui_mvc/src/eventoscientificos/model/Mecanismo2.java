/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Mecanismo2 implements MecanismoDistribuicao {

    @Override
    public void distribuir(ProcessoDistribuicao pd) {
        List<Submissao> listSub = pd.getEvento().getListaSubmissoes();
        
        List<Revisor> listRev = pd.getEvento().getCP().getListaRevisores();
        
        boolean adicionarRev=false, equit=false;
        int dialogResult = JOptionPane.showConfirmDialog (null, "Distribuir Equitativamente as Revisões?","Mecanismo 2",JOptionPane.YES_NO_OPTION);
        if(dialogResult == JOptionPane.YES_OPTION){
           equit=true;
           int nrRev = listRev.size()/listSub.size();
        }
        
        for (Submissao s : listSub) {
            Distribuicao d = new Distribuicao();
            d.setArtigo(s.getArtigo());
            adicionarRev = false;
            for (Revisor r : listRev) {
                for (Topico t : r.getListaTopicos()) {
                    if(s.getArtigo().getListTopicosArtigo().contains(t)) {
                        adicionarRev = true;
                    }
                }
            }
        }
        
    }
    
    @Override
    public String descricaoMecanismo() {
        return "Mecanismo2: Pelo menos 2 revisores por artigo, pelo menos um com afinidade ao artigo.";
    }
}
