/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Gustavo
 */
public class Persistencia implements Serializable {

    
     /**
     * Para nao andar a pedir ao utilizador sempre que se fecha o programa aonde quer guardar o ficheiro de texto
     * criei uma variavel de classe pathLU (caminho da pasta lista de utilizadores) para quando fechar o programa guardar la.
     *
     */
    static String pathLU;

    /**
     * Carrega a lista de Utilizadores apartir de um "localFicheiroLU"
     * devolve uma string com o caminho do ficheiro a ser lido pelo programa.
     *
     * @return Uma lista (List) com utilizadores de uma dada empresa.
     */
    public static List<Utilizador> carregarDadosUtilizador() {
        Persistencia.pathLU = localFicheiroLU();

        List<Utilizador> listaUtilizadores = new ArrayList<>();
        try {
            Scanner in = new Scanner(new File(Persistencia.pathLU));
            String e;
            String[] a;

            while (in.hasNext()) {
                e = in.nextLine();
                a = e.split(";");
                listaUtilizadores.add(new Utilizador(a[1], a[2], a[0], a[3]));
            }
            in.close();

        } catch (FileNotFoundException e) {
            System.out.println("Não existe lista de testes, (ListaUtilizadores.txt- na pasta do programa)");
        } catch (NullPointerException e) {
            listaUtilizadores = new ArrayList();
        }
        for (int i = 0; i < listaUtilizadores.size(); i++) {
            System.out.println(listaUtilizadores.get(i));
        }
        return listaUtilizadores;
    }

    /**
     * Metodo que vai interagir com o user, que retorna o caminho para o ficheio
     * que o utilizador quer que venha ser adicionado a empresa como lista de
     * utilizador ou da a opção de usar o ficheiro de teste (esse ficheiro tem o nome
     *ListaUtilizadores.txt) e esta na pasta do projecto.
     *
     * @return caminho do ficheiro em questão.
     */
    public static String localFicheiroLU() {

        int reply = JOptionPane.showConfirmDialog(null, "Deseja usar de testes?", "Lista Utilizador", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_NO_OPTION) {

            return "ListaUtilizadores.txt";
        } else {
            JFileChooser chooser = new JFileChooser();
            File f = new File("c:/");
            File namePath;
            chooser.setCurrentDirectory(f);
            int checker;
            checker = chooser.showOpenDialog(null);
            if (checker == JFileChooser.APPROVE_OPTION) {

                namePath = chooser.getSelectedFile();

                return namePath.getAbsolutePath();
            } else {
                return null;
            }
        }
    }

    /**
     * metodo que vai escrever num ficheiro a lista de utilizadores.
     *
     * @param lu a lista de utilizadores.
     */
    public static void gravarFicheiros(List<Utilizador> lu) {
        if (Persistencia.pathLU == null&&!(lu.isEmpty())) {

            // se a patch for null entao é pk ainda nao foi selecionado um ficheiro
            //deve de dar nome ao ficheiro e depois escolher o sitio aonde o guardar
            int reply = JOptionPane.showConfirmDialog(null, "Deseja guardar os dados?", "Saida", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_NO_OPTION) {
                Persistencia.pathLU = localFicheiroLU();
            }
        }
        if (!(Persistencia.pathLU == null)) {

            String escrever;

            try {
                Formatter out = new Formatter(new File(Persistencia.pathLU));
                for (int i = 0; i < lu.size(); i++) {
                    escrever = lu.get(i).getNome() + ";" + lu.get(i).getUsername() + ";" + lu.get(i).getPassword() + ";" + lu.get(i).getEmail();
                    out.format("%s%n", escrever);
                    out.flush();
                }
                out.close();

            } catch (FileNotFoundException e) {
                System.out.println("Erro a gravar ficheiro de utilizador no disco.");
            }
        }
    }

}
