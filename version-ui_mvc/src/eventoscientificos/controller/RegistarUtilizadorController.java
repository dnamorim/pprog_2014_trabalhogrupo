package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


/**
 *
 * @author Nuno Silva
 */

public class RegistarUtilizadorController
{
    private Empresa m_empresa;
    private Utilizador m_utilizador;

    public RegistarUtilizadorController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void novoUtilizador()
    {
        m_utilizador = m_empresa.novoUtilizador();
    }

    public Utilizador setDados(String strNome, String strUsername, String strPassword, String strEmail)
    {
        m_utilizador.setNome(strNome);
        m_utilizador.setUsername(strUsername);
        m_utilizador.setPassword(strPassword);
        m_utilizador.setEmail(strEmail);
        
        
        if( m_empresa.registaUtilizador(m_utilizador) )
            return m_utilizador;
        else
            return null;
    }
    
    public void importUsers(String file) throws FileNotFoundException {
        Scanner in = new Scanner(new File(file), "UTF-8");
        
        
        /**
         * Exemplo:
         * nome;username;password;email
         */
        while(in.hasNext()) {
            novoUtilizador();
            String[] aux = in.nextLine().split(";");
            setDados(aux[0], aux[1], aux[2], aux[3]);
        }
        
    }
}

