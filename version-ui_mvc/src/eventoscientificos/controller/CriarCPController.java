package eventoscientificos.controller;


import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Nuno Silva
 */

public class CriarCPController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private CP m_cp;

    public CriarCPController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public List<Evento> getEventosOrganizador(String strId)
    {
        return m_empresa.getEventosOrganizador(strId);
    }
    
    public List<Topico> getTopicosEvento()
    {
        if( m_evento != null )
            return m_evento.getTopicos();
        else
            return null;
    }
    
    public void selectEvento(Evento e)
    {
        m_evento = e;
        
        m_cp = m_evento.novaCP();
    }
    
    public Revisor addMembroCP(String strId)
    {
        Utilizador u = m_empresa.getUtilizador(strId);
        
        if( u!=null)
            return m_cp.addMembroCP( strId, u );
        else
            return null;
    }
    
    public boolean registaMembroCP( Revisor r )
    {
        return m_cp.registaMembroCP(r);
    }
    
    public void setCP()
    {
        m_evento.setCP(m_cp); 
    }
    
    public void setListaTopicosRevisor(Revisor r, List<Topico> listaTopicosRevisor)
    {
        if( m_cp.getListaRevisores().contains(r) )
            r.setListaTopicos( listaTopicosRevisor );
    }
    
    
   public void importCP(String file, Evento evento) throws FileNotFoundException {
       Scanner in = new Scanner(new File(file), "UTF-8");
       selectEvento(evento);
       
       while(in.hasNext()) {
           String[] aux = in.nextLine().split(";");
           Revisor r = addMembroCP(aux[0]); 
           
           List<Topico> lstTopicosRev = new ArrayList<Topico>();
           for (int i = 1; i < aux.length; i++) {
               for (Topico t : this.getTopicosEvento()) {
                   if(t.getCodigoACM().equals(aux[i])) {
                       lstTopicosRev.add(t);
                   }
               }
           }
           registaMembroCP(r);
           setListaTopicosRevisor(r, lstTopicosRev);
       }
       setCP();
   }
}

