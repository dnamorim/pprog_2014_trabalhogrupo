package eventoscientificos.controller;

import eventoscientificos.controller.CriarTopicoEventoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.lang.IndexOutOfBoundsException;

/**
 *
 * @author Nuno Silva
 */

public class CriarEventoCientificoController
{
    private Empresa m_empresa;
    private Evento m_evento;

    /**
     * Instância do Controlador CriarEventoCientifico para uma empresa passada por parâmetro
     * @param empresa 
     */
    public CriarEventoCientificoController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    /**
     * Cria uma Instância de um Novo Evento 
     */
    public void novoEvento()
    {
        m_evento = m_empresa.novoEvento();
    }

    /**
     * Devolve a informação do evento no Controlador
     * @return infoEvento
     */
    public String getEventoString()
    {
        return m_evento.toString();
    }

    /**
     * Define o título do Evento
     * @param strTitulo  tituloEvento
     */
    public void setTitulo(String strTitulo)
    {
        m_evento.setTitulo(strTitulo);
    }
    
    /**
     * Define a descrição do evento
     * @param strDescricao descricaoEvento
     */
    public void setDescricao(String strDescricao)
    {
        m_evento.setDescricao(strDescricao);
    }

    /**
     * Define o local do evento
     * @param strLocal localEvento 
     */
    public void setLocal(String strLocal)
    {
        m_evento.setLocal(strLocal);
    }
    
    /**
     * Define a data de inicio do evento
     * @param strDataInicio dataInicio 
     */
    public void setDataInicio(String strDataInicio)
    {
        m_evento.setDataInicio(strDataInicio);
    }

    /**
     * Define a data de fim do evento
     * @param strDataFim dataFim
     */
    public void setDataFim(String strDataFim)
    {
        m_evento.setDataFim(strDataFim);
    }
    
    /**
     * Define a data limite de submissões do evento
     * @param strDataLimiteSubmissão dataLimiteSubmissaoEvento
     */
    public void setDataLimiteSubmissão(String strDataLimiteSubmissão)
    {
        m_evento.setDataLimiteSubmissão(strDataLimiteSubmissão);
    }

    /**
     * Adiciona um Organizador/Utilizador ao Evento
     * @param strId username do organizador
     * @return resultadoOperacao (true=sucesso / false=insucesso)
     */
    public boolean addOrganizador(String strId)
    {
        Utilizador u = m_empresa.getUtilizador(strId);
        
        if( u!=null)
            return m_evento.addOrganizador( strId, u );
        else
            return false;
    }
    
    /**
     * Instancia o evento registado utilizando o controlador
     * @return resultadoOperacao (true=sucesso/false=insucesso)
     */
    public boolean registaEvento()
    {
        return m_empresa.registaEvento(m_evento);
    }
    
    /**
     * Mecanismo de importação de eventos a partir de um ficheiro de texto
     * @param fileToOpen path do ficheiro txt
     * @throws FileNotFoundException 
     */
    public void importEvents(String fileToOpen) throws FileNotFoundException {
        Scanner in = new Scanner(new File(fileToOpen), "UTF-8");
        
        //Exemplo Ficheiro:
        //titulo;descricao;dataInicio;dataFim;dataLimiteSubmissao;local
        //nomeOrg1;usernameOrg1;...
        //topic1;topic2;...
        
        while(in.hasNext()) {
            String[] auxEvt = in.nextLine().split(";");
            String[] auxOrg = in.nextLine().split(";");
            String[] auxTop = in.nextLine().split(";");
            
            novoEvento();
            this.setTitulo(auxEvt[0]);
            this.setDescricao(auxEvt[1]);
            this.setDataInicio(auxEvt[2]);
            this.setDataFim(auxEvt[3]);
            this.setDataLimiteSubmissão(auxEvt[4]);
            this.setLocal(auxEvt[5]);
            
            strToOrganizadores(auxOrg);
            strToTopicos(auxTop);
            registaEvento();
        }
    }
    
    /**
     * Adiciona vários Organizadores ao evento
     * @param aux listaDeOrganizadores(vector)
     */
    public void strToOrganizadores(String[] aux) {
        for (int i = 0; i < aux.length; i++) {
            addOrganizador(aux[i]);
        }
    }
    
    public void strToTopicos(String[] aux) {
        CriarTopicoEventoController ctec = new CriarTopicoEventoController(m_empresa);
        ctec.setEvento(m_evento);
        if(aux.length%2 == 0) {
            for (int i = 0; i < aux.length; i=i+2) {
                Topico t = ctec.addTopico(aux[i], aux[i+1]);
                ctec.registaTopico(t);
            }
        }
        
    }
}

