/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;


import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Topico;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SubmeterArtigoController
{

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private Artigo m_artigo;

    public SubmeterArtigoController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public List<Evento> iniciarSubmissao()
    {
        return this.m_empresa.getListaEventosPodeSubmeter();
    }

    public void selectEvento(Evento e)
    {
        m_evento = e;
        this.m_submissao = this.m_evento.novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }

    public List<Topico> getTopicosEvento()
    {
        if (m_evento != null)
        {
            return m_evento.getTopicos();
        } else
        {
            return null;
        }
    }

    public void setDados(String strTitulo, String strResumo)
    {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    public Autor novoAutor(String strNome, String strAfiliacao)
    {
        return this.m_artigo.novoAutor(strNome, strAfiliacao);
    }
    
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail)
    {
        return this.m_artigo.novoAutor(strNome, strAfiliacao,strEmail,this.m_empresa.getUtilizadorEmail(strEmail));
    }

    public boolean addAutor(Autor autor)
    {
        return this.m_artigo.addAutor(autor);
    }

    public List<Autor> getPossiveisAutoresCorrespondentes()
    {
        return this.m_artigo.getPossiveisAutoresCorrespondentes();
    }

    public void setCorrespondente(Autor autor)
    {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    public void setFicheiro(String strFicheiro)
    {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    public String getInfoResumo()
    {
        return this.m_submissao.getInfo() + this.m_artigo.getInfo();
    }

    public boolean registarSubmissao()
    {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_evento.addSubmissao(m_submissao);
    }

    // adicionado na iteração 2
    public void setListaTopicosArtigo(List<Topico> listaTopicosArtigo)
    {
        m_artigo.setListaTopicos(listaTopicosArtigo);
    }

    public void importSubmissoes(String file, Evento e) throws FileNotFoundException {
        Scanner in = new Scanner(new File(file), "UTF-8");
        
        while(in.hasNext()) {
            this.selectEvento(e);
            String[] auxArt = in.nextLine().split(";");
            String[] auxAut = in.nextLine().split(";");
            String[] auxTop = in.nextLine().split(";");
            
            setDados(auxArt[0], auxArt[1]);
            this.setFicheiro(auxArt[2]);
            
            Autor aCor = this.novoAutor(auxArt[3], auxArt[4], auxArt[5]);
            this.addAutor(aCor);
            this.setCorrespondente(aCor);
            
            
            for (int i = 0; i < auxAut.length; i=i+2) {
                this.addAutor(this.novoAutor(auxAut[i], auxAut[i+1]));
            }
            
            List<Topico> lstTopicos = new ArrayList<Topico>();
            for (int i = 0; i < auxTop.length; i++) {
               for (Topico t : this.m_evento.getListTopicos()) {
                   if(t.getCodigoACM().equals(auxTop[i])) {
                       lstTopicos.add(t);
                   }
               }
           }
            setListaTopicosArtigo(lstTopicos);
            this.registarSubmissao();
        }
    }
}
