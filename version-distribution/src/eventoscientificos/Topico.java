/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 *
 * @author iazevedo
 */
public class Topico implements Serializable{
    
    private String m_strDescricao;
    
    private String m_strCodigoACM;
    
    /**
     *
     * @param descricao
     * @param codigoACM
     */
    public Topico (String descricao,String codigoACM){
        this.m_strDescricao=descricao;
        this.m_strCodigoACM=codigoACM;
    }

    /**
     *
     * @param strDescricao
     */
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    /**
     *
     * @param codigoACM
     */
    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }
    
    /**
     *
     * @return
     */
    public boolean valida()
    {
        return true;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        return "\nCodigo: " + this.m_strCodigoACM +"\nDescrição: " + this.m_strDescricao;
    }

    /**
     *
     * @return
     */
    public String getM_strDescricao() {
        return m_strDescricao;
    }

    /**
     *
     * @param m_strDescricao
     */
    public void setM_strDescricao(String m_strDescricao) {
        this.m_strDescricao = m_strDescricao;
    }

    /**
     *
     * @return
     */
    public String getM_strCodigoACM() {
        return m_strCodigoACM;
    }

    /**
     *
     * @param m_strCodigoACM
     */
    public void setM_strCodigoACM(String m_strCodigoACM) {
        this.m_strCodigoACM = m_strCodigoACM;
    }
    
}
