/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Autor implements Serializable{

    private Utilizador m_Utilizador;
    private String m_strNome;
    private String m_strAfiliacao;
    private String m_strEMail;

    /**
     * constroi um autor novo sem parametros
     */
    public Autor() {
        this.m_Utilizador = null;
    }

    /**
     * altera o nomo de um autor
     *
     * @param strNome recebe o nome de um autor
     */
    public void setNome(String strNome) {
        this.m_strNome = strNome;
    }

    /**altera a afiliacao de um autor
     *
     * @param strAfiliacao recebe a afiliacao do autor.
     */
    public void setAfiliacao(String strAfiliacao) {
        this.m_strAfiliacao = strAfiliacao;
    }

    /**
     * altera o email de um autor
     *
     * @param strEMail recebe o email de um autor.
     */
    public void setEMail(String strEMail) {
        this.m_strEMail = strEMail;
    }

    /**
     * altera o utilizador a qual o autor esta relacionado
     *
     * @param utilizador recebe o utilizador do autor.
     */
    public void setUtilizador(Utilizador utilizador) {
        this.m_Utilizador = utilizador;
    }


    /**
     * metodo que devolde a descricao do autor
     *
     * @return descricao do autor
     */
    @Override
    public String toString() {
        return this.m_strNome + " - " + this.m_strAfiliacao + " - " + this.m_strEMail;
    }
}
