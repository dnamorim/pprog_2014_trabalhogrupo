package eventoscientificos;


import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Nuno Silva
 */

public class CP implements Serializable
{
    List<Revisor> m_listaRevisores;

    /**
     * constroi um CP sem parametros
     *
     */
    public CP()
    {
        m_listaRevisores = new ArrayList();
    }

    /**
     * adiciona a CP um membro novo
     *
     * @param strId nome do membro da cp
     * @param u utilizador a qual ele corresponde.
     * @return
     */
    public Revisor addMembroCP( String strId, Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
    
    private boolean validaMembroCP(Revisor r)
    {
        return true;
    }
    
    /**
     * regista o membro na CP
     *
     * @param r Revisor a ser adicionado.
     * @return
     */
    public boolean registaMembroCP(Revisor r)
    {
        return m_listaRevisores.add(r);
    }
    
    /**
     * descreve a CP
     *
     * @return descrisao da CP
     */
    @Override
    public String toString()
    {
        String strCP = "Membros de CP: ";
        for( ListIterator<Revisor> it = m_listaRevisores.listIterator(); it.hasNext(); )
        {
            strCP += it.next().toString();
            if( it.hasNext() )
                strCP += ", ";
        }
        return strCP;
    }

    /**
     * devolve a lsita de revisores dentro da CP
     *
     * @return lsita de Revisores da CP
     */
    public List<Revisor> getListaRevisores() 
    {
        return this.m_listaRevisores;
    }
    
    /**
     * altara a lista de revisores da CP
     *
     * @param lr lista de revisores da CP
     */
    public void setListaRevisores(List<Revisor> lr){
        this.m_listaRevisores=lr;
    }
}