/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Distribuicao {
    
    private Artigo artigo;
    private List<Revisor> listRevisoresArtigo;
    private MecanismoDistribuicao m_MecanismosDistribuição;
    
   
    
    /**
     *
     */
    public Distribuicao() {
        this.listRevisoresArtigo = new ArrayList<Revisor>();
    }
    
    /**
     *
     * @param artigo
     * @param listRevisores
     */
    public Distribuicao(Artigo artigo, List<Revisor> listRevisores) {
        this.artigo = artigo;
        this.listRevisoresArtigo = listRevisores;

    }
    
    /**
     *
     * @param artigo
     */
    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }
    
    /**
     *
     * @param revisor
     * @return
     */
    public boolean addRevisor(Revisor revisor) {
        return this.listRevisoresArtigo.add(revisor);
    }
    

}
