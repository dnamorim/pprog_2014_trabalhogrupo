package eventoscientificos;

import java.io.Serializable;
import java.util.*;
import javax.swing.JOptionPane;

/**
 *
 * @author Nuno Silva
 */
public class Empresa implements Serializable {

    /**
     * Lista de Utilizadores de uma empresa
     */
    public List<Utilizador> m_listaUtilizadores;

    /**
     * Lista de Evento de uma empresa
     */
    public List<Evento> m_listaEventos;

    /**
     * processos de distribuir de uma empresa.
     */
    public static ProcessoDistribuicao m_processoDistribuicao;

    /**
     * Constroi uma Variavel de classe empresa sem paramentros.
     */
    public Empresa() {
        m_listaUtilizadores = new ArrayList<>();
        levantarDadosUtilizador();

        m_listaEventos = new ArrayList<>();

    }

    /**
     * Cria um novo utilizador
     *
     * @return Um novo Utilizador
     */
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }



  
    private boolean validaUtilizador(Utilizador u) {
        for (Utilizador uExistente : m_listaUtilizadores) {
            if (uExistente.mesmoQueUtilizador(u)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Cria um novo Evento
     *
     * @return novo evento.
     */
    public Evento novoEvento() {
        return new Evento();
    }



    private boolean validaEvento(Evento e) {
        return true;
    }

    /**
     * retorna um utilizador apartir do valor de referencia dele no Array de Utilizadores
     *
     * @param strId valor de Referencia no Array de utilizadores
     * @return Utilizador selecionado.
     */
    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getUsername();
            if (s1.equalsIgnoreCase(strId)) {
                return u;
            }
        }
        return null;
    }

    /**
     * retorna utilizador atravez do email.
     * procura pela lista um utilizador com o email introduzido e devolve esse
     * utilizador
     *
     * @param strEmail para procurar pelo array
     * @return utilizador encontrado.
     */
    public Utilizador getUtilizadorEmail(String strEmail) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strEmail)) {
                return u;
            }
        }

        return null;
    }





    /**
     *
     * @param strId
     * @return
     */
    public List<Evento> getEventosOrganizador(String strId) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = getUtilizador(strId);

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    /**
     *
     * @return
     */
    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_listaEventos) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }

    /**
     *
     * @return
     */
    public List<Utilizador> getListaUtilizador() {
        return this.m_listaUtilizadores;
    }

    /**
     *
     */
    public void apresentarListaUtilizadores() {
        for (int i = 0; i < getListaUtilizador().size(); i++) {
            System.out.println(getListaUtilizador().get(i).toString());
        }
    }

    /**
     *
     */
    public void guardaListaUtilizador() {
     //  apresentarListaUtilizadores();

        //   Persistencia.gravarFicheiroUtilizadores(getListaUtilizador());
    }

    /**
     *
     */
    public void guardaListaEventosSemTopicos() {
        apresentarListaEventoSemTopicos();

        //  Persistencia.gravarFicheiroEventosSemTopicos(m_listaEventos);
    }

    /**
     *
     */
    public void apresentarListaEventoSemTopicos() {
        for (int i = 0; i < m_listaEventos.size(); i++) {
            System.out.println(m_listaEventos.get(i).toString());
        }

    }

    /**
     *
     */
    public void levantarDadosUtilizador() {
        List<Utilizador> lu = Persistencia.levantarDadosUtilizador();
        m_listaUtilizadores = lu;
    }

    /**
     *
     */
    public void importarListaEventos() {
        List<Evento> le = Persistencia.CarregarDadosEvento(m_listaEventos, m_listaUtilizadores);
        m_listaEventos = le;

    }

    /**
     *
     */
    public void importarListaUtilizador() {
        m_listaUtilizadores = Persistencia.carregarDadosUtilizador(m_listaUtilizadores);

    }

    /**
     *
     * @return
     */
    public List<Evento> getListaEventos() {
        return this.m_listaEventos;
    }

    /**
     *
     */
    public void listarEventos() {
        String escrever = "Eventos:\n";
        for (int i = 0; i < m_listaEventos.size(); i++) {
            escrever = escrever + "\n " + i + ": " + m_listaEventos.get(i).getTitulo() + ", " + m_listaEventos.get(i).getDescricao();

        }
        JOptionPane.showMessageDialog(null, escrever);
    }

    void listarUtilizadores() {
        String escrever = "Utilizadores: \n";
        for (int i = 0; i < m_listaUtilizadores.size(); i++) {
            escrever = escrever + "\n " + m_listaUtilizadores.get(i).getNome() + ", " + m_listaUtilizadores.get(i).getEmail();
        }
        JOptionPane.showMessageDialog(null, escrever);
    }

    /**
     *
     * @param evento
     * @param mecanismo
     */
    public void novoProcessoDeDistribuicao(Evento evento,MecanismoDistribuicao mecanismo) {
        ProcessoDistribuicao m_processDistribuicao = new ProcessoDistribuicao();
        
        m_processDistribuicao.setEvento(evento);
        m_processDistribuicao.setMecanismoDistribuicao(mecanismo);
        m_processDistribuicao.distribuir();

    }

}
