/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import static com.sun.org.apache.regexp.internal.RETest.test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static jdk.nashorn.internal.objects.NativeRegExp.test;

/**
 *
 * @author Gustavo
 */
public class Persistencia implements Serializable {

    private Empresa m_empresa;
    private Evento m_evento;

    /**
     *
     * @param m_empresa
     */
    public Persistencia(Empresa m_empresa) {
        this.m_empresa = m_empresa;

    }

    /**
     * Para nao andar a pedir ao utilizador sempre que se fecha o programa aonde
     * quer guardar o ficheiro de texto criei uma variavel de classe pathLU
     * (caminho da pasta lista de utilizadores) para quando fechar o programa
     * guardar la.
     *
     */
    static String pathLU = "ListaUtilizadores.txt";

    static String pathLE;

    /**
     *
     * @return
     */
    public static List<Utilizador> levantarDadosUtilizador() {
        List<Utilizador> listaUtilizadores = new ArrayList<>();
        try {
            Scanner in = new Scanner(new File(Persistencia.pathLU));
            String e;
            String[] a;

            while (in.hasNext()) {
                e = in.nextLine();
                a = e.split(";");
                listaUtilizadores.add(new Utilizador(a[1], a[2], a[0], a[3]));
            }
            in.close();

        } catch (FileNotFoundException e) {
            System.out.println("Não existe lista de testes");
            listaUtilizadores = new ArrayList();
        } catch (NullPointerException e) {
            listaUtilizadores = new ArrayList();
        }

        return listaUtilizadores;

    }

    /**
     * Carrega a lista de Utilizadores apartir de um "localFicheiroLU" <-
     * devolve uma string com o caminho do ficheiro a ser lido pelo programa.
     *
     * @param lu
     * @return Uma lista (List) com utilizadores de uma dada empresa.
     */
    public static List<Utilizador> carregarDadosUtilizador(List<Utilizador> lu) {
        Persistencia.pathLU = localFicheiro();

        if (Persistencia.pathLU == null) {
            return lu;
        } else {

            try {
                Scanner in = new Scanner(new File(Persistencia.pathLU));
                String e;
                String[] a;

                while (in.hasNext()) {
                    e = in.nextLine();
                    a = e.split(";");
                    lu.add(new Utilizador(a[1], a[2], a[0], a[3]));
                }
                in.close();

            } catch (FileNotFoundException e) {
                System.out.println("Não existe lista de testes, (ListaUtilizadores.txt- na pasta do programa)");
            } catch (NullPointerException e) {
                lu = new ArrayList();
            }

            return lu;
        }
    }

    /**
     * Metodo que vai interagir com o user, que retorna o caminho para o ficheio
     * que o utilizador quer que venha ser adicionado a empresa como lista de
     * utilizador ou da a opção de usar o ficheiro de teste (esse ficheiro tem o
     * nome ListaUtilizadores.txt) e esta na pasta do projecto.
     *
     * @return caminho do ficheiro em questão.
     */
    public static String localFicheiro() {

        JFileChooser chooser = new JFileChooser();
        File f = new File("c:/");
        File namePath;
        chooser.setCurrentDirectory(f);
        int checker;
        checker = chooser.showOpenDialog(null);
        if (checker == JFileChooser.APPROVE_OPTION) {

            namePath = chooser.getSelectedFile();

            return namePath.getAbsolutePath();
        } else {
            return null;
        }

    }

/////
    /**
     *
     * @param listaEventos
     * @param lu
     * @return
     */
    public static List<Evento> CarregarDadosEvento(List<Evento> listaEventos, List<Utilizador> lu) {
        List<Organizador> lo;

        try {
            Persistencia.pathLE = localFicheiro();

            Scanner in = new Scanner(new File(Persistencia.pathLE));
            String e;
            String[] a;
            String[] b;

            while (in.hasNext()) {
                lo = new ArrayList<>();
                e = in.nextLine();

                a = e.split(";");
                b = a[7].split(",");

                for (int i = 0; i < b.length; i++) {

                    for (int j = 0; j < lu.size(); j++) {
                        if (b[i].equals(lu.get(j).getEmail())) {
                            lo.add(new Organizador(lu.get(j).getNome(), lu.get(j)));
                        }
                    }

                }
                listaEventos.add(new Evento(a[0], a[1], a[2], a[3], a[4], a[5], a[6], lo));

            }
            in.close();

        } catch (FileNotFoundException e) {
            System.out.println("Erro no Carregar dados evento");
        } catch (NullPointerException e) {

        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "Ficheio não esta formatado como deve de ser. tente outro ficheiro.");
        }

        for (int i = 0; i < listaEventos.size(); i++) {
            System.out.println(listaEventos.get(i).toString());
        }

        return listaEventos;
    }

    static List<Topico> CarregarTopicosParaEvento() {
        String pathTopico = localFicheiro();
        List<Topico> lt = new ArrayList<>();
        try {
            Scanner in = new Scanner(new File(pathTopico));

            String e;
            String[] a;

            while (in.hasNext()) {
                e = in.nextLine();

                a = e.split(";");

                lt.add(new Topico(a[0], a[1]));

            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Ficheiro invalido");
        } catch (NullPointerException e) {

        }

        return lt;
    }

    /**
     *
     * @param evento
     * @return
     */
    public static List<Revisor> CarregarCP(Evento evento) {
        List<Revisor> lr = new ArrayList<>();

        String pathCP = localFicheiro();

        try {
            Scanner in = new Scanner(new File(pathCP));

            String e;
            String[] a;
            String[] b;

            while (in.hasNext()) {
                e = in.nextLine();

                a = e.split(";");
                b = a[4].split(",");
                List<Topico> ltr = new ArrayList<>();
                for (int i = 0; i < b.length; i++) {
                    for (int j = 0; j < evento.getListaTopicos().size(); j++) {
                        if (b[i].equals(evento.getListaTopicos().get(j).getM_strDescricao())) {
                            ltr.add(evento.getListaTopicos().get(j));
                        }
                    }

                }
                lr.add(new Revisor(new Utilizador(a[1], a[2], a[0], a[3]), ltr));

            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Ficheiro invalido");
        }

        return lr;
    }

    /**
     *
     * @param evento
     * @return
     */
    public static List<Submissao> carregarListaSubmissoes(Evento evento) {
        List<Submissao> ls = new ArrayList<>();

        String pathLS = localFicheiro();

        try {
            Scanner in = new Scanner(new File(pathLS));

            String e;
            String[] a;
            String[] b;

            while (in.hasNext()) {
                e = in.nextLine();

                a = e.split(";");
                b = a[1].split(",");
                List<Topico> lta = new ArrayList<>();
                for (int i = 0; i < b.length; i++) {
                    for (int j = 0; j < evento.getListaTopicos().size(); j++) {
                        if (b[i].equals(evento.getListaTopicos().get(j).getM_strDescricao())) {
                            lta.add(evento.getListaTopicos().get(j));
                        }
                    }

                }
                ls.add(new Submissao(new Artigo(a[0], lta)));

            }

        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Ficheiro invalido");
        } catch (NullPointerException e) {

        }

        return ls;
    }

    /**
     *
     * @param evento
     */
    public static void gravarEvento(Evento evento) {

        String nomeEvento = evento.getTitulo() + ".bin";
        try {
            try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(nomeEvento))) {
                out.writeObject(evento);

                out.flush();
                out.close();
            }

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Erro a gravar o ficheiro");
            e.printStackTrace();

        }
    }

    public static Evento importarEventoDistribuido() {
        Evento evento;

        String pathED = localFicheiro();
        try {
            FileInputStream fis = new FileInputStream(pathED);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            evento=(Evento) ois.readObject();

            ois.close();

        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Erro a carregar ficheio");
            evento=null;
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "O ficheiro nao esta correcto");
            evento=null;
        }
        
        return evento;
    }

}
