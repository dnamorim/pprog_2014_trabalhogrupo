/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Artigo implements Serializable{

    private String m_strTitulo;
    private String m_strResumo;
    private List<Autor> m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_strFicheiro;
    private List<Topico> m_listaTopicos;
    private List<Revisor> m_listaRevisor;

    /**
     * Construtor de Artigo sem parametros. Listas vazias.
     *
     */
    public Artigo(){
        m_listaAutores = new ArrayList<Autor>();
        m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     * Construtor de Artigo recebendo o titulo e um lista de topicos.
     *
     * @param titulo Titulo do Artigo.
     * @param lt Lista de Tópicos do artigo.
     */
    public Artigo(String titulo, List<Topico> lt) {
        this.m_strTitulo = titulo;
        this.m_listaTopicos = lt;
        this.m_listaRevisor = new ArrayList<>();
    }

    /**
     * Altera o titulo de um artigo
     *
     * @param strTitulo recebe o titulo de um artigo. (String)
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    /**
     * Altera o resumo de um artigo
     *
     * @param strResumo recebe o novo resumo do artigo (String)
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     * Cria um novo autor e adiciona ao Artigo.
     *
     * @param strNome nome do Autor (String)
     * @param strAfiliacao nome da afiliação do autor (String).
     * @return retorna um novo autor (Autor)
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);

        return autor;
    }

    /**
     * Cria um novo autor.
     *
     * @param strNome nome do Autor
     * @param strAfiliacao afiliação do autor
     * @param strEmail Email do autor
     * @param utilizador Utilizador a qual se refenre (Class Utilizador)
     * @return
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail, Utilizador utilizador) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);
        autor.setUtilizador(utilizador);

        return autor;
    }

    /**
     * metodo que coloca a lista de topicos num artigo
     *
     * @param listaTopicos A lista de topicos a colocar (List<Topicos>)
     */
    public void setListaTopicos(List<Topico> listaTopicos) {
        this.m_listaTopicos.addAll(listaTopicos);
    }

    /**
     * retorna o (String) do titulo do Artigo
     *
     * @return String com o titulo do artigo.
     */
    public String getM_strTitulo() {
        return m_strTitulo;
    }

    /**
     * altera valor da String m_strTitulo
     *
     * @param m_strTitulo rebece o novo titulo
     */
    public void setM_strTitulo(String m_strTitulo) {
        this.m_strTitulo = m_strTitulo;
    }

    /**
     * devolve a lista de topicos de um artigo.
     *
     * @return List<Topico> de um artigo
     */
    public List<Topico> getM_listaTopicos() {
        return m_listaTopicos;
    }

    /**
     * altera a lista de topicos de um artigo
     *
     * @param m_listaTopicos recebe a nova lsita de topicos do artigo.
     */
    public void setM_listaTopicos(List<Topico> m_listaTopicos) {
        this.m_listaTopicos = m_listaTopicos;
    }

    /**
     * devolde a lista de revisor de um artigo
     *
     * @return a lista de revisor de um artigo
     */
    public List<Revisor> getM_listaRevisor() {
        return m_listaRevisor;
    }

}
