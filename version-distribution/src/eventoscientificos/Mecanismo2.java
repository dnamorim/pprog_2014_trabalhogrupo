/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Mecanismo2 implements MecanismoDistribuicao {

    /**
     *
     */
    public Mecanismo2() {

    }

    @Override
    public void distribuir(ProcessoDistribuicao pd) {
        limparListaRevisoresArtigo(pd.getEvento());
        
        List<Submissao> listSub = pd.getEvento().getListaSubmissoes();

        List<Revisor> listRev = pd.getEvento().getCP().getListaRevisores();

        //Adicionar todos os revisores com 
        //Corro a lista de Revisores a procura de um revisor com um topico de afinidade com o artigo
        //se encontrotar adiciono atoda a lista de revisores a esse artigo.
        //Atribuit pelo menos 2 Revisores a um artigo (certo)
        //Pelos menos um Revisor ter afinidade com o artigo (certo)
        // Criterio opcional: tds os revisores terem o mesmo numeo de topicos (certo)
        boolean adicionarRev;

        for (int i = 0; i < listSub.size(); i++) {
            adicionarRev = false;

            for (int j = 0; j < listRev.size(); j++) {

                //corre todos os topicos do revisor e verifica se o revisor tem esse topico.
                for (int z = 0; z < listRev.get(j).getM_listaTopicos().size(); z++) {

                    //se o o topcio de revisor esta dentro da lsita de topicos de artigo.
                    if (listSub.get(i).getArtigo().getM_listaTopicos().contains(listRev.get(j).getM_listaTopicos().get(z))) {
                        adicionarRev = true;

                    }
                }

            }
            if (adicionarRev == true) {
                listSub.get(i).getArtigo().getM_listaRevisor().addAll(listRev);
            }
        }

    }

    @Override
    public String descricaoMecanismo() {
        return "Mecanismo2: Pelo menos 2 revisores por artigo, pelo menos um com afinidade ao artigo.";
    }

    /**
     *
     * @param evento
     */
    @Override
    public void limparListaRevisoresArtigo(Evento evento) {
                for (int i = 0; i < evento.getListSubmissoes().size(); i++) {

            evento.getListSubmissoes().get(i).getArtigo().getM_listaRevisor().clear();

        }
    }
}
