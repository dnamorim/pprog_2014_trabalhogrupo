/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class Mecanismo1 implements MecanismoDistribuicao {

    /**
     *
     */
    public Mecanismo1() {

    }

    @Override
    public void distribuir(ProcessoDistribuicao pd) {
        limparListaRevisoresArtigo(pd.getEvento());
        List<Submissao> listSub = pd.getEvento().getListaSubmissoes();

        List<Revisor> listRev = pd.getEvento().getCP().getListaRevisores();

        boolean adicionarRev;

        for (int i = 0; i < listSub.size(); i++) {

            for (int j = 0; j < listRev.size(); j++) {
                adicionarRev = false;

                //corre todos os topicos de artigo e verifica se o revisor tem esse topico.
                for (int z = 0; z < listRev.get(j).getM_listaTopicos().size(); z++) {

                    //se o topico esta dentro do topico de pericia do revisor e se o revisor nao esta ja dentro da lista de revisores do artigo entao fazer.
                    if (listSub.get(i).getArtigo().getM_listaTopicos().contains(listRev.get(j).getM_listaTopicos().get(z))) {
                        adicionarRev = true;

                    }
                }
                if (adicionarRev == true && listSub.get(i).getArtigo().getM_listaRevisor().size() < 3) {
                    listSub.get(i).getArtigo().getM_listaRevisor().add(listRev.get(j));
                }
            }
        }

        for (int l = 0; l < listSub.size(); l++) {
            System.out.println(listSub.get(l).getArtigo().getM_strTitulo() + " tem " + listSub.get(l).getArtigo().getM_listaRevisor().size() + " revisores");
        }

    }

    @Override
    public String descricaoMecanismo() {
        return "Mecanismo 1: Todos os topicos tem preferencialmente 3 revisores";
    }

    /**
     *
     * @param evento
     * @param evento
     */
    public void limparListaRevisoresArtigo(Evento evento) {

        for (int i = 0; i < evento.getListSubmissoes().size(); i++) {

            evento.getListSubmissoes().get(i).getArtigo().getM_listaRevisor().clear();

        }

    }
}
