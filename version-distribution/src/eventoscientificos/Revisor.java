/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */
public class Revisor implements Serializable{

    private String m_strNome;
    private Utilizador m_utilizador;

    // adicionada na iteração 1
    private List<Topico> m_listaTopicos = new ArrayList<Topico>();

    /**
     *
     * @param u
     */
    public Revisor(Utilizador u) {
        m_strNome = u.getNome();
        m_utilizador = u;
    }

    /**
     *
     * @param u
     * @param listaTopico
     */
    public Revisor(Utilizador u, List<Topico> listaTopico) {
        m_strNome = u.getNome();
        m_utilizador = u;
        m_listaTopicos = listaTopico;
    }

    // adicionada na iteração 2

    /**
     *
     * @param listaTopicos
     */
        public void setListaTopicos(List<Topico> listaTopicos) {
        m_listaTopicos.addAll(listaTopicos);
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        return true;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     *
     * @return
     */
    public Utilizador getUtilizador() {
        return m_utilizador;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {

        String escreve = "";
        for (int i = 0; i < this.getM_listaTopicos().size(); i++) {
            escreve = escreve+" " + this.getM_listaTopicos().get(i).getM_strDescricao();
        }
        return m_strNome +", " + escreve;
    }

    /**
     *
     * @return
     */
    public List<Topico> getM_listaTopicos() {
        return m_listaTopicos;
    }

    /**
     *
     * @param m_listaTopicos
     */
    public void setM_listaTopicos(List<Topico> m_listaTopicos) {
        this.m_listaTopicos = m_listaTopicos;
    }

}
