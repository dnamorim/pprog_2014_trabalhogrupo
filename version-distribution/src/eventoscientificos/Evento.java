/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Nuno Silva
 */
public class Evento implements Serializable{

    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private String m_strDataInicio;
    private String m_strDataFim;
    private String m_strDataLimiteSubmissão;
    ///NOVO**
    private String m_strNumeroMaximoTopicos;
//    private String m_strDataLimiteSubmissãoFinal;
    //
    private List<Organizador> m_listaOrganizadores;
    private List<Submissao> m_listaSubmissoes;
    private CP m_cp;
    private List<Topico> m_listaTopicos;

    /**
     *
     */
    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     *
     * @param titulo
     * @param descricao
     */
    public Evento(String titulo, String descricao) {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
        this.setTitulo(titulo);
        this.setDescricao(descricao);
    }

    /**
     *
     * @param titulo
     * @param descricao
     * @param local
     * @param dataIni
     * @param dataFin
     * @param dataSubm
     * @param numMaxTop
     * @param lo
     */
    public Evento(String titulo, String descricao, String local, String dataIni, String dataFin, String dataSubm, String numMaxTop, List<Organizador> lo) {
        this.m_strTitulo = titulo;
        this.m_strDescricao = descricao;
        this.m_local = new Local(local);
        this.m_strDataInicio = dataIni;
        this.m_strDataFim = dataFin;
        this.m_strDataLimiteSubmissão = dataSubm;
        this.m_strNumeroMaximoTopicos = numMaxTop;
        this.m_listaOrganizadores = lo;
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     *
     * @return
     */
    public String getTitulo() {
        return m_strTitulo;
    }

    /**
     *
     * @param m_strTitulo
     */
    public void setTitulo(String m_strTitulo) {
        this.m_strTitulo = m_strTitulo;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return m_strDescricao;
    }

    /**
     *
     * @param m_strDescricao
     */
    public void setDescricao(String m_strDescricao) {
        this.m_strDescricao = m_strDescricao;
    }

    /**
     *
     * @return
     */
    public Local getLocal() {
        return m_local;
    }

    /**
     *
     * @param m_local
     */
    public void setLocal(Local m_local) {
        this.m_local = m_local;
    }

    /**
     *
     * @return
     */
    public String getDataInicio() {
        return m_strDataInicio;
    }

    /**
     *
     * @param m_strDataInicio
     */
    public void setDataInicio(String m_strDataInicio) {
        this.m_strDataInicio = m_strDataInicio;
    }

    /**
     *
     * @return
     */
    public String getDataFim() {
        return m_strDataFim;
    }

    /**
     *
     * @param m_strDataFim
     */
    public void setDataFim(String m_strDataFim) {
        this.m_strDataFim = m_strDataFim;
    }

    /**
     *
     * @return
     */
    public String getDataLimiteSubmissão() {
        return m_strDataLimiteSubmissão;
    }

    /**
     *
     * @param m_strDataLimiteSubmissão
     */
    public void setDataLimiteSubmissão(String m_strDataLimiteSubmissão) {
        this.m_strDataLimiteSubmissão = m_strDataLimiteSubmissão;
    }

    /**
     *
     * @return
     */
    public String getNumeroMaximoTopicos() {
        return m_strNumeroMaximoTopicos;
    }

    /**
     *
     * @param m_strNumeroMaximoTopicos
     */
    public void setNumeroMaximoTopicos(String m_strNumeroMaximoTopicos) {
        this.m_strNumeroMaximoTopicos = m_strNumeroMaximoTopicos;
    }

    /**
     *
     * @return
     */
    public List<Organizador> getListaOrganizadores() {
        return m_listaOrganizadores;
    }

    /**
     *
     * @param m_listaOrganizadores
     */
    public void setListaOrganizadores(List<Organizador> m_listaOrganizadores) {
        this.m_listaOrganizadores = m_listaOrganizadores;
    }

    /**
     *
     * @return
     */
    public List<Topico> getListaTopicos() {
        return m_listaTopicos;
    }

    /**
     *
     * @param m_listaTopicos
     */
    public void setListaTopicos(List<Topico> m_listaTopicos) {
        this.m_listaTopicos = m_listaTopicos;
    }

    /**
     *
     * @return
     */
    public CP novaCP() {
        m_cp = new CP();

        return m_cp;
    }

    /**
     *
     * @param strLocal
     */
    public void setLocal(String strLocal) {
        this.m_local.setLocal(strLocal);
    }

    /**
     *
     * @param u
     * @return
     */
    public boolean addOrganizador(Utilizador u) {
        Organizador o = new Organizador(u.getNome(), u);

        o.valida();
        for (Organizador m_listaOrganizadore : m_listaOrganizadores) {
            if (m_listaOrganizadore.getUtilizador().getEmail().equals(o.getUtilizador().getEmail())) {
                return false;
            }
        }

        return addOrganizador(o);

    }

    private boolean addOrganizador(Organizador o) {
        return m_listaOrganizadores.add(o);
    }

    /**
     *
     * @return
     */
    public boolean valida() {
        return true;
    }

    /**
     *
     * @param cp
     */
    public void setCP(CP cp) {
        m_cp = cp;
    }

    //Trabalhar nisto..
    ////

    /**
     *
     * @return
     */
        @Override
    public String toString() {
        String devolve;
        devolve = this.m_strTitulo + ": " + this.m_strDescricao + "\n De " + this.m_strDataInicio + " ate " + this.m_strDataFim
                + "\n Organizado por:";

        for (int i = 0; i < m_listaOrganizadores.size(); i++) {
            devolve = devolve + "\n" + m_listaOrganizadores.get(i).getNome() + ", " + m_listaOrganizadores.get(i).getUtilizador().getEmail();
        }
        return devolve;
    }

////

    /**
     *
     * @return
     */
        public boolean aceitaSubmissoes() {
        return true;
    }

    /**
     *
     * @param submissao
     * @return
     */
    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.m_listaSubmissoes.add(submissao);
        } else {
            return false;
        }
    }

    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }

    /**
     *
     * @return
     */
    public List<Submissao> getListaSubmissoes() {
        return this.m_listaSubmissoes;
    }

    /**
     *
     * @return
     */
    public CP getCP() {
        return this.m_cp;
    }

    /**
     *
     * @return
     */
    public List<Topico> getTopicos() {
        return m_listaTopicos;
    }


    // adicionada na iteração 2

    /**
     *
     * @param t
     * @return
     */
        public boolean addTopico(Topico t) {
        return this.m_listaTopicos.add(t);
    }

    // adicionada na iteração 2

    /**
     *
     * @param t
     * @return
     */
        public boolean validaTopico(Topico t) {
        if (t.valida() && validaGlobalTopico(t)) {
            return true;
        } else {
            return false;
        }
    }

    // adicionada na iteração 2
    private boolean validaGlobalTopico(Topico t) {
        return true;
    }

    /**
     *
     */
    public void importarTopicos() {
        this.m_listaTopicos=Persistencia.CarregarTopicosParaEvento();
    }

    /**
     *
     */
    public void importarCP() {
        this.novaCP();
        this.getCP().setListaRevisores(Persistencia.CarregarCP(this));
    }
    
    /**
     *
     */
    public void importarSubmissoes(){
        this.setListaSubmissoes(Persistencia.carregarListaSubmissoes(this));
        
    }
    
    /**
     *
     * @param listaSubmissao
     */
    public void setListaSubmissoes(List<Submissao> listaSubmissao){
        this.m_listaSubmissoes=listaSubmissao;
    }
    
    /**
     *
     * @return
     */
    public List<Submissao> getListSubmissoes(){
        return this.m_listaSubmissoes;
    }
}
