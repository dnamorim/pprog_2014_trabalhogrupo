/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public interface MecanismoDistribuicao {
    
    /**
     *
     * @param pd
     */
    public void distribuir(ProcessoDistribuicao pd);
    
    /**
     *
     * @return
     */
    public String descricaoMecanismo();
    
    /**
     *
     * @param evento
     */
    public void limparListaRevisoresArtigo(Evento evento);

}
