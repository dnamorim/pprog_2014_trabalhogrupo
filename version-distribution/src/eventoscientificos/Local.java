/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */

public class Local implements Serializable
{
    private String m_strLocal;

    /**
     *
     */
    public Local()
    {
    }

    /**
     *
     * @param local
     */
    public Local(String local)
    {
        this.m_strLocal=local;
    }
    
    /**
     *
     * @param strLocal
     */
    public void setLocal(String strLocal)
    {
        m_strLocal = strLocal;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        return m_strLocal;
    }
}
