/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.io.Serializable;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Submissao implements Serializable
{
    private Artigo m_artigo;

    /**
     *
     * @param artigo
     */
    public Submissao(Artigo artigo)
    {
    this.m_artigo=artigo;
    }
    
    /**
     *
     * @return
     */
    public Artigo novoArtigo()
    {
        return new Artigo();
    }
    
    /**
     *
     * @return
     */
    public String getInfo()
    {
        return this.toString();
    }

    /**
     *
     * @return
     */
    public Artigo getArtigo() {
        return m_artigo;
    }

    /**
     *
     * @param m_artigo
     */
    public void setArtigo(Artigo m_artigo) {
        this.m_artigo = m_artigo;
    }
    
    /**
     *
     * @return
     */
    public boolean valida()
    {
        return true;
    }
    
    /**
     *
     * @return
     */
    @Override
    public String toString()
    {
        String escrever="";
        for(int i =0;i<this.m_artigo.getM_listaTopicos().size();i++){
            escrever = escrever + " "+this.m_artigo.getM_listaTopicos().get(i).getM_strDescricao();
            
        }
        return this.m_artigo.getM_strTitulo()+", "+escrever;
    }
    
    /**
     *
     * @return
     */
    public String toStringRevisores() {
        String escrever ="";
        for (int i =0;i <this.m_artigo.getM_listaRevisor().size();i++){
            escrever = escrever+", "+this.m_artigo.getM_listaRevisor().get(i).getNome();
        }
        
        return this.m_artigo.getM_strTitulo()+escrever;
        
    }
}
