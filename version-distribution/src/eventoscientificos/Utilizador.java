package eventoscientificos;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */

public final class Utilizador implements Serializable
{
    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;

    /**
     *
     */
    public Utilizador()
    {
    }
    
    /**
     *
     * @param username
     * @param pwd
     * @param nome
     * @param email
     */
    public Utilizador(String username, String pwd, String nome, String email)     
    {
        this.setUsername(username);
        this.setPassword(pwd);
        this.setNome(nome);
        this.setEmail(email);
    }
    
    /**
     *
     * @param strNome
     */
    public final void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }

    /**
     *
     * @param strUsername
     */
    public final void setUsername(String strUsername)
    {
        m_strUsername = strUsername;
    }

    /**
     *
     * @param strPassword
     */
    public final void setPassword(String strPassword)
    {
        m_strPassword = strPassword;
    }

    /**
     *
     * @param strEmail
     */
    public void setEmail(String strEmail)
    {
        this.m_strEmail = strEmail;
    }

    /**
     *
     * @return
     */
    public boolean valida()
    {
        return true;
    }
    
    // método alterado na iteração 2

    /**
     *
     * @param u
     * @return
     */
        public boolean mesmoQueUtilizador(Utilizador u)
    {
        if( getUsername().equals( u.getUsername() ) ||
            getEmail().equals( u.getEmail() ) )
            return true;
        else
            return false;
    }
    
    

    /**
     *
     * @return
     */
        public String getNome()
    {
        return m_strNome;
    }
    
    /**
     *
     * @return
     */
    public String getUsername()
    {
        return m_strUsername;
    }
    
    /**
     *
     * @return
     */
    public String getEmail()
    {
        return m_strEmail;
    }
    

    /**
     *
     * @return
     */
        public String getPassword(){
        return this.m_strPassword;
    }
    
    /**
     * descreve o utilizador
     *
     * @return descricao o utilizador
     */
    @Override
    public String toString()
    {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.m_strNome + "\n";
        str += "\tUsername: " + this.m_strUsername + "\n";
        str += "\tPassword: " + this.m_strPassword + "\n";
        str += "\tEmail: " + this.m_strEmail + "\n";

        return str;
    }
    
            
}

