/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class ProcessoDistribuicao {

    private MecanismoDistribuicao mecanismo;
    private List<Distribuicao> listDistribuicao;
    private Evento evento;
    
    /**
     *
     */
    public ProcessoDistribuicao() {
        listDistribuicao = new ArrayList<Distribuicao>();
  
    }
    
    /**
     *
     * @param listDistribuicao
     */
    public void setListDistribuicao(List<Distribuicao> listDistribuicao) {
        this.listDistribuicao = listDistribuicao;
    }
    
    /**
     *
     * @param artigo
     * @param listRevisores
     */
    public void novaDistribuicao(Artigo artigo, List<Revisor> listRevisores) {
        this.listDistribuicao.add(new Distribuicao(artigo, listRevisores));
    }
    
    /**
     *
     * @param e
     */
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    /**
     *
     * @param mecanismo
     */
    public void setMecanismoDistribuicao(MecanismoDistribuicao mecanismo) {
        this.mecanismo = mecanismo;
    }
    
    /**
     *
     * @return
     */
    public Evento getEvento() {
        return evento;
    }
    
    /**
     *
     */
    public void distribuir() {
        this.mecanismo.distribuir(this);
    }
    
}

